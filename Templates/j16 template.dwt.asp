<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stop Junction 16</title>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<link href="../favicon/Favicon.ico" rel="shortcut icon" title="Favicon" type="image/x-icon" />
<style type="text/css">
body {
	background-image: url(../backgrounds/grass%20texture.jpg);
}
body table {
	background-image: url(../backgrounds/text%20background.png);
}
</style>
</head>

<body onload="MM_preloadImages('../buttons/home button inverted.png','../buttons/history button inverted.png','../buttons/correspondence button inverted.png','../buttons/Resistence files button inverted.png','../banners/website banner inverted.fw.png')">
<table width="100%" border="0" align="center">
  <tr align="center" valign="middle">
    <td><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('banner','','../banners/website banner inverted.fw.png',1)"><img src="../banners/website banner.jpg" name="banner" width="100%" id="banner" /></a></tr>
  <tr>
    <td align="center" valign="middle"><a href="../home page.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','../buttons/home button inverted.png',1)"><img src="../buttons/home button.png" name="Home" width="200" height="50" border="0" id="Home" /></a><a href="../history page.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('History','','../buttons/history button inverted.png',1)"><img src="../buttons/history button.png" alt="History" name="History" width="200" height="50" border="0" id="History" /></a><a href="../correspondence page.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Correspondence','','../buttons/correspondence button inverted.png',1)"><img src="../buttons/correspondence button.png" alt="Correspondence" name="Correspondence" width="200" height="50" border="0" id="Correspondence" /></a><a href="../resistence files page.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Resistence files','','../buttons/Resistence files button inverted.png',1)"><img src="../buttons/Resistence files button.png" alt="Resistence files" name="Resistence files" width="200" height="50" border="0" id="Resistence files" /></a></td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td><!-- TemplateBeginEditable name="Content" --><!-- TemplateEndEditable --></td>
  </tr>
</table>
</body>
</html>
